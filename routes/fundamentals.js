const express = require("express");
const router = express.Router();

require("dotenv").config();
const variableData = process.env.variableData || "Fundamentals";

router.get("/", function (req, res, next) {
  res.send({
    name: "Fundamentals",
    server: "express",
    variableData: variableData,
  });
});

module.exports = router;
